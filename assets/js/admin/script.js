(function($){

  if(jQuery('.selectize').length > 0)
  {
    var $select = jQuery('.selectize').selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false
    });
  }

  /*Save widget titles */
  jQuery("button#wraw_add_titles").click(function(e){
    e.preventDefault();
    e.stopPropagation();

    var country_code = jQuery("#wraw_widget_friendly_title_localization").val();
    var widget_title = jQuery("#wraw_widget_friendly_title").val();
    var widget_articles = jQuery("#wraw_articles").val();
    var widget_on_articles = jQuery("#wraw_on_articles").val();

    jQuery.ajax({
              type: "POST",
              url: wraw_admin.ajax_url,
              data: {
                widget_id : wraw_admin.widget_id,
                country_code: country_code,
                widget_title: widget_title,
                widget_articles: widget_articles,
                widget_on_articles: widget_on_articles,
                action: wraw_admin.widget_save_widget_action
              },
              success: function (response) {
                  var message = "<strong>NOTE: It might fail to save/update if the title or articles are the not changed!</strong><br>";
                  if(response['status_title'] != false)
                  {
                    message += "Title saved!<br>";
                    var color   = "#dff0d8";
                    var fontColor = "#3c763d";
                  }
                  else
                  {
                    message += "Failed to save title!<br>";
                    var color   = "#f2dede";
                    var fontColor = "#a94442";
                  }

                  if(response['status_articles'] != false)
                  {
                    message += "Articles saved!";
                    var color   = "#dff0d8";
                    var fontColor = "#3c763d";
                  }
                  else
                  {
                    message += "Failed to save articles!";
                    var color   = "#f2dede";
                    var fontColor = "#a94442";
                  }

                  var notification = jQuery("#wraw_wrapper .alert");
                  notification.html(message);
                  notification.css(
                    {
                      "background" : color,
                      "color" : fontColor
                    }
                  );
                  notification.show();
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  console.log(errorThrown);
              }
          });
  });

  /* Switch widget title */
    jQuery("#wraw_widget_friendly_title_localization").change(function(){

        var country_code = jQuery(this).val();
        jQuery("#wraw_wrapper .alert").hide();
        jQuery.ajax({
              url: wraw_admin.ajax_url,
              type: "GET",
              dataType: 'json',
              data: {
                widget_id : wraw_admin.widget_id,
                country_code: country_code,
                action: wraw_admin.widget_title_action
              },
              success: function (response) {
                  $select[0].selectize.clear();
                  jQuery("#wraw_widget_friendly_title").val(response['title']);
                  var articles = "";
                  jQuery.each(response['articles'], function(k,article){
                      $select[0].selectize.addOption({
                        text: article['title'],
                        value: article['id']
                      }
                    );
                    $select[0].selectize.addItem(article['id']);
                  });
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  console.log(errorThrown);
              }
          });
    });

  /* Change Ad No Utm From Ads List */
  jQuery(".wraw_default_no_utm").change(function(){
      if(jQuery(this).is(":checked"))
      {
        var status = 1;
      }
      else
      {
        var status = 0;
      }

      var widget_id = jQuery(this).data('widget-id');

      jQuery.ajax({
            type: "POST",
            url: wraw_admin.ajax_url,
            data: {
              status: status,
              post_id: widget_id,
              action: wraw_admin.wraw_default_no_utm_action
            },
            success: function (response) {
                console.log(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    /* Change Widget Status */
    jQuery(".widgets_status").change(function(){
        if(jQuery(this).is(":checked"))
        {
          var status = 1;
        }
        else
        {
          var status = 0;
        }

        var widget_id = jQuery(this).data('widget-id');

        jQuery.ajax({
              type: "POST",
              url: wraw_admin.ajax_url,
              data: {
                status: status,
                post_id: widget_id,
                action: wraw_admin.widget_status_action
              },
              success: function (response) {
                  console.log(response);
              },
              error: function (jqXHR, textStatus, errorThrown) {
                  console.log(errorThrown);
              }
          });
    });

  /* Change Widget Visibility From Widget List */
  jQuery(".wraw_widget_visibility").change(function(){
      if(jQuery(this).is(":checked"))
      {
        var vis_status = 1;
      }
      else
      {
        var vis_status = 0;
      }

      var widget_id = jQuery(this).data('widget-id');

      jQuery.ajax({
            type: "POST",
            url: wraw_admin.ajax_url,
            data: {
              status: vis_status,
              post_id: widget_id,
              action: wraw_admin.wraw_widget_visibility_action
            },
            success: function (response) {
                console.log(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    /** Search Post Dialog */
     $('body').on('click', '#wraw_search_articles_btn, #wraw_search_on_articles_btn', function(event) {
            if($(this).attr('id') == 'wraw_search_on_articles_btn') {
              wpActiveEditor = 'wraw_on_articles';
              wpLink.open();
              $("#wp-link-submit").addClass('wraw_on_all_posts');
              $("#wp-link-submit").removeClass('wraw_all_posts');
            }
            else {
              wpActiveEditor = 'wraw_articles';
              wpLink.open();
              $("#wp-link-submit").addClass('wraw_all_posts');
              $("#wp-link-submit").removeClass('wraw_on_all_posts');
            }
            return false;
        });

    $('body').on('click', '.wraw_all_posts, .wraw_on_all_posts', function(event) {

            var linkAtts = wpLink.getAttrs();
            var linkPostID = -1;

            var show_widget_on = false;

            if($(this).hasClass('wraw_on_all_posts')) {
              show_widget_on = true;
            }

            $.ajax({
              url: wraw_admin.ajax_url,
              type: 'post',
              dataType: 'json',
              data : {
                action: 'wraw_get_post_id_by_url',
                url : linkAtts.href
              },
              success: function(postID){
                linkPostID = postID;  
                title = "";
                title = $("li.selected:first .item-title").text();
                if(show_widget_on){
                  $select[1].selectize.addOption({
                      text: title,
                      value: linkPostID
                    }
                  );
                  $select[1].selectize.addItem(linkPostID);
                } else {
                  $select[0].selectize.addOption({
                        text: title,
                        value: linkPostID
                      }
                    );
                  $select[0].selectize.addItem(linkPostID);
                }
              },
              error: function(error){
                alert(error);
              }
            }).done(function(){
              wpLink.textarea = $('body'); 
              wpLink.close();
            }).fail(function(){
              wpLink.textarea = $('body'); 
              wpLink.close();
            });

           
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            event.stopPropagation();
            return false;
        });

        

  $('body').on('click', '#wp-link-cancel, #wp-link-close', function(event) {
        wpLink.textarea = $('body');
        wpLink.close();
        event.preventDefault ? event.preventDefault() : event.returnValue = false;
        event.stopPropagation();
        return false;
    });


  $("#wraw_external_articles_btn").click(function(){
    clearModal();
    $(".wraw-modal.wraw-external-article").css("display", "block");
  });

   /* Open modal for selecting images */

  var template = `
    <div class="wraw-row">
          <div class="wraw-external-article-image" style="background-image: url(#src#);">
            <span>X</span>
          </div>
          <input type="hidden" name="_wraw_external_article_image_src" value="#src#" >
          <input type="hidden" name="_wraw_external_article_image_id" value="#id#" >
        </div>
  `;

  var file_frame;
  $('.wraw-select-image').on('click', function( event ){

    event.preventDefault();
    event.stopPropagation();
    if ( file_frame ) {
      file_frame.open();
      return;
    }

    file_frame = wp.media.frames.file_frame = wp.media({
      title: $( this ).data( 'uploader_title' ),
      button: {
        text: $( this ).data( 'uploader_button_text' ),
      },
      multiple: false 
    });

    var rows = "";

    file_frame.on( 'select', function() {
      // Get all, not just the first
      attachment = file_frame.state().get('selection').toJSON();
      for(var i = 0; i < attachment.length; i++) {
         var image = attachment[i];
         var src = image.url;
         var id  = image.id;
         row = template.replace(/#src#/g, src);
         row = row.replace("#id#", id);
         rows = row;
      }
      $(".wraw-image-preview").html(rows);
      $(".wraw-image-preview").css("display","block");
    });

    file_frame.open();
  });
  /* End modal */
  $(document).on("click",".wpdo-first-slide-o-selected-images .wpdo-fs-o-img", function(event){
    $(this).parent().remove();
  });
  $(document).on("click",".wraw-external-article-add-button", function(event){
    event.preventDefault();
    event.stopPropagation();
    var title = $("input[name='wraw_widget_external_article_title']").val();
    var subtitle = $("input[name='wraw_widget_external_article_subtitle']").val();
    var link  = $("input[name='wraw_widget_external_article_link']").val();
    var image  = $("input[name='_wraw_external_article_image_src']").val();
    var extra  = $("input[name='wraw_widget_external_article_extra']").val();
    $select[0].selectize.addOption({
        text: title,
        value: [title, link, image, extra, subtitle]
      }
    );
    $select[0].selectize.addItem([title, link, image, extra, subtitle]);
    var $edit = $("#wraw_widget_articles").find(".wraw-external-article-edit");
    if($edit.length) $edit.children('a').trigger('click');

    $(".wraw-image-preview").html("");
    $(".wraw-image-preview").css("display","none");
    $(".wraw-modal.wraw-external-article").css("display", "none");

  });
  $(document).on("click",".wraw-external-article-cancel-button", function(event){
    event.preventDefault();
    event.stopPropagation();
    $("#wraw_widget_articles").find(".wraw-external-article-edit").map(function(){ $(this).removeClass("wraw-external-article-edit")});
    $(".wraw-image-preview").html("");
    $(".wraw-image-preview").css("display","none");
    $(".wraw-modal.wraw-external-article").css("display", "none");
  });
  $(document).on("click",".wraw-external-article-image span", function(event){
    event.preventDefault();
    event.stopPropagation();
    $(".wraw-image-preview").html("");
    $(".wraw-image-preview").css("display","none");
  });
  
  $(document).on("click","#wraw_widget_articles .selectize-input .item", function(e){
    var value = $(this).data('value');
    if(isNumeric(value) || e.target.className == 'remove') return;

    var fields = value.split(',');

    var title = $("input[name='wraw_widget_external_article_title']").val(fields[0]);
    var link  = $("input[name='wraw_widget_external_article_link']").val(fields[1]);
    var image  = $("input[name='_wraw_external_article_image_src']").val(fields[2]);
    var extra  = $("input[name='wraw_widget_external_article_extra']").val(fields[3]);
    var subtitle  = $("input[name='wraw_widget_external_article_subtitle']").val(fields[4]);
    var src = fields[2];
    row = template.replace(/#src#/g, src);
    rows = row;

    $(".wraw-image-preview").html(rows);
    $(".wraw-image-preview").css("display","block")
    $(".wraw-external-article-add-button").text("Update Article");
    $(this).addClass('wraw-external-article-edit');
    $(".wraw-modal.wraw-external-article").css("display", "block");
  });

  if($("#wraw_widget_template").val() == '1x1'){
    $("#wraw_action_button").show();
  }
  $("#wraw_widget_template").change(function(){
    if($(this).val() == '1x1')
    {
      $("#wraw_action_button").show();
    }else{
      $("#wraw_action_button").hide();
    }
  });

  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  function clearModal()
  {
    var title = $("input[name='wraw_widget_external_article_title']").val('');
    var subtitle = $("input[name='wraw_widget_external_article_subtitle']").val('');
    var link  = $("input[name='wraw_widget_external_article_link']").val('');
    var image  = $("input[name='_wraw_external_article_image_src']").val('');
    var extra  = $("input[name='wraw_widget_external_article_extra']").val('');
    $(".wraw-external-article-add-button").text("Add Article");
  }

})(jQuery)