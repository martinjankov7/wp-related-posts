function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};



(function($){
  $(function(){
    if($(".wraw-slider").length > 0)
    {
      jQuery(".wraw-slider-close").click(function(){
        jQuery(this).parents('.wraw-slider').css('display','none');
      });
      var position = $(".wraw-slider[style^='left']").length > 0 ? "left" : "right";

      var onScroll = debounce(function () {
        if(jQuery(document).scrollTop() > 200){
          if(position == "left")
          {
            jQuery('.wraw-slider').animate({
              'left': "5px"
            }, 'fast');
          }
          else
          {
            jQuery('.wraw-slider').animate({
              'right': "5px"
            }, 'fast');
          }
        }else{
          if(position == "left")
          {
            jQuery('.wraw-slider').animate({
              "left": "-320px"
            }, 'fast');
          }
          else
          {
            jQuery('.wraw-slider').animate({
              "right": "-320px"
            }, 'fast');
          }
        }
      }, 100);

      jQuery(window).on('scroll', onScroll);
      }
  });
})(jQuery);
