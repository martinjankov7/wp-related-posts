<?php
/**
 * WPRP_Related_Widget
 *
 * @package    WPRelatedPosts
*/
class WPRP_Related_Widget {

	public function __construct() {

	}

	/**
	 * Get post type custom field value
	 * @param  string $field
	 * @param  int $ID
	 * @return string
	 */
	public static function get( string $field_name, int $ID = null ) {
		if ( is_null( $ID ) ) {
			$ID = get_the_ID();
		}

		global $post;

		$meta_key = '_' . $field_name;

		return get_post_meta( $ID, $meta_key, true );
	}
}
