<?php

// Creating the widget 
class WRAW_WP_Widget extends \WP_Widget {
	function __construct() {
		parent::__construct(

			'wraw_related_posts_widget', 

			__('Related Posts Widget'), 

			array( 'description' => __( 'Widget to display related posts'), )
		);

		$this->init();
	}

	public function widget( $args, $instance ) {
		$wraw_id = $instance['wraw_id'];
		echo $args['before_widget'];
		echo do_shortcode('[wraw_related_posts id="'.$wraw_id.'" title="'.$instance['title'].'"]');
		echo $args['after_widget'];
	}
		
	public function form( $instance ) {
		if(get_post_meta($instance['wraw_id'],'_wraw_related_articles_active_switch', true))
		{
			$title = apply_filters( 'widget_title', $instance['wraw_id'] );

			if ( isset( $instance[ 'wraw_id' ] )) {
				$wraw_id = $instance[ 'wraw_id' ];
			}
			else {
				$wraw_id = '';
			}
		}
		else
		{
			$title = "No widgets or deactivated!";
		}
		
		?>
		<p>
		<label><?php _e( 'Select Widget:' ); ?></label> 

		<select class="widefat selectize" id="<?php echo $this->get_field_id( 'wraw_id' ); ?>" name="<?php echo $this->get_field_name( 'wraw_id' ); ?>" type="text">
		
		<?php	
		$all_widgets = new \WP_Query(array(
				'post_type'	=> 'wraw_widgets',
				'post_status' => 'publish',
				'posts_per_page'	=> -1,
				'meta_key'	=> '_wraw_related_articles_active_switch',
				'meta_compare' => 'EXISTS'
			));
		wp_reset_query();
		if ( $all_widgets->have_posts() ) 
		{
			while($all_widgets->have_posts())
			{
				$all_widgets->the_post();
				if($wraw_id==get_the_ID()) {
					echo '<option value="'.$wraw_id.'" selected>'.get_the_title().'</option>'."\n";
					$title = get_the_title();
					$device = get_post_meta($wraw_id, '_wraw_device' ,true);
				}
				else {
					echo '<option value="'.get_the_ID().'">'.get_the_title().'</option>'."\n";
				}
			}
			wp_reset_postdata();
		}
		?>			
		</select>
		</p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="hidden" value="<?php echo esc_attr( $title ); ?>" />
	<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['wraw_id'] = ( ! empty( $new_instance['wraw_id'] ) ) ? strip_tags( $new_instance['wraw_id'] ) : '';
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}		

	public function wraw_load_widget() {
		register_widget( "WRAW_WP_Widget" );
	}

	public function init()
	{
		add_action( 'widgets_init', array($this,'wraw_load_widget' ));
	}
}

?>