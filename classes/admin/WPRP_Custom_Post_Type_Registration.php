<?php
/**
 * Register Custom Post Type
 *
 * @package    WPRelatedPosts
*/
class WPRP_Custom_Post_Type_Registration {

	public function __construct() {

		add_action( 'init', array($this,'register') );
		add_action( 'admin_enqueue_scripts', array($this,'wprp_admin_assets') );
		add_action(	'save_post', array($this,'wprp_save_widget'),10,2);
		add_action( 'manage_wp_related_widgets_posts_custom_column', array($this,'wprp_widget_list_columns_content'), 10, 2);

		add_filter( 'manage_wp_related_widgets_posts_columns', array($this,'wprp_widget_list_columns'), 10);
	}

	public function register() {
		$labels = array(
			'name'               => __( 'Related Posts Widgets'),
			'singular_name'      => __( 'Related Post Widgets'),
			'add_new'            => __( 'Create New Widget '),
			'add_new_item'       => __( 'Create New Widget' ),
			'edit_item'          => __( 'Edit Widget' ),
			'new_item'           => __( 'Create New Widget' ),
			'all_items'          => __( 'All Widgets' ),
			'view_item'          => __( 'View Widget' ),
			'search_items'       => __( 'Search Widgets' ),
			'not_found'          => __( 'No Widgets found' ),
			'not_found_in_trash' => __( 'No Widgets found in the Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => 'Related Posts Widgets'
		);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Widgets for showing realted articles',
			'public'        => true,
			'menu_position' => 5,
			'supports'      => array( 'title','editor'),
			'has_archive'   => true,
			'capabilities' => array(
				    'edit_post'          => 'edit_wprp_widgets',
				    'read_post'          => 'read_wprp_widgets',
				    'delete_post'        => 'delete_wprp_widgets',
				    'edit_posts'         => 'edit_wprp_widgets',
				    'edit_others_posts'  => 'edit_others_wprp_widgets',
				    'delete_posts'       => 'delete_wprp_widgets',
				    'publish_posts'      => 'publish_wprp_widgets',
				    'read_private_posts' => 'read_private_wprp_widgets'
				),
			'register_meta_box_cb' => array( $this,'metaboxes' )
		);
		register_post_type( 'wp_related_widgets', $args );
	}

	public function metaboxes()
	{
		add_meta_box( 'wprp_related_articles', 'Related Post Widget', array( $this, 'related_post_widgets' ), 'wp_related_widgets', 'normal', 'high' );
		add_meta_box( 'wprp_related_articles_sidebar', 'Widget Status', array($this,'related_post_widgets_sidebar' ), 'wp_related_widgets', 'side', 'high' );
	}

	public function related_post_widgets() {
		// Noncename needed to verify where the data originated
		echo '<input type="hidden" name="wprp_noncename" id="wprp_ noncename" value="' . wp_create_nonce( plugin_basename( WPRP_PLUGIN_FILE) ) .  '"  />';

		@include  WPRP_PLUGIN_DIR . 'views/admin/template-custom-post-type-metabox.php';
	}

	public function related_post_widgets_sidebar() {
		@include  WPRP_PLUGIN_DIR . 'views/admin/template-custom-post-type-metabox-sidebar.php';
	}

	public function admin_assets($hook)  {
	 	global $post;

		if ( ! isset( $post ) ) return;

		if ( $post->post_type !== "wp_related_widgets" &&
			( ! isset($_GET['post_type'] ) || ( isset( $_GET['post_type'] ) && $_GET['post_type'] !== 'wp_related_widgets') )
		) {
			return;
		}

		wp_enqueue_script('wp-link');

		wp_enqueue_style( 'wprp-selectize-style', WPRP_PLUGIN_URL . 'libraries/selectize/dist/css/selectize.css', array(), null );
		wp_register_script( 'wprp-selectize-script', WPRP_PLUGIN_URL . 'libraries/selectize/dist/js/standalone/selectize.min.js', array('jquery'), null,true );
	}

	public function save_widget( $post_id, $post ) {
		if ( ! isset( $_POST['wprp_noncename'] ) ) {
			return $post_id;
		}

		if ( ! wp_verify_nonce( $_POST['wprp_noncename'], plugin_basename( WPRP_PLUGIN_FILE ) ) ) {
			return $post->ID;
		}

		if ( ! current_user_can( 'edit_post', $post->ID ) ) {
			return $post->ID;
		}

		$wprp_meta['_wprp_widget_size']				= $_POST['wprp_widget_size'];
		$wprp_meta['_wprp_widget_friendly_title']	= $_POST['wprp_widget_friendly_title'];
		$wprp_meta['_wprp_device']					= $_POST['wprp_device'];
		$wprp_meta['_wprp_widget_articles']			= $_POST['wprp_widget_articles'];
		$wprp_meta['_wprp_widget_on_articles']			= $_POST['wprp_widget_on_articles'];
		$wprp_meta['_wprp_widget_article_title_length']		= $_POST['wprp_widget_article_title_length'];
		$wprp_meta['_wprp_widget_template']			= $_POST['wprp_widget_template'];
		$wprp_meta['_wprp_action_button_text']			= $_POST['wprp_action_button_text'];
		$wprp_meta['_wprp_slider_box_position']		= $_POST['wprp_slider_box_position'];
		$wprp_meta['_wprp_utm_source']				= $_POST['wprp_utm_source'];
		$wprp_meta['_wprp_utm_medium']				= $_POST['wprp_utm_medium'];
		$wprp_meta['_wprp_utm_campaign']			= $_POST['wprp_utm_campaign'];
		$wprp_meta['_wprp_widget_color']			= $_POST['wprp_widget_color'];
		$wprp_meta['_wprp_widget_show_on_pages']			= $_POST['wprp_widget_show_on_pages'];
		$wprp_meta['_wprp_widget_show_on_pages_repeat']			= $_POST['wprp_widget_show_on_pages_repeat'];
		$wprp_meta['_wprp_widget_slides_number']	= $_POST['wprp_widget_slides_number'];

		$this->_save_post_meta( $post, $wprp_meta );

		delete_transient( 'wprp_widget_' . $post_id );
	}

	public function widget_list_columns( $defaults ) {
		return array(
			'widget_status' => __( 'Widget Status', 'wprp' ),
	    'title' => __( 'Title', 'wprp' ),
	    'shortcode' => __( 'Shortcode', 'wprp' ),
	    'size' => __( 'Widget Size', 'wprp' ),
			'displayon' => __( 'Display On Device', 'wprp' ),
	    'date' 			=> __( 'Date', 'wprp' )
	  );
	}

 	public function widget_list_columns_content( $column_name, $post_ID ) {
		if ( $column_name == 'widget_status' ) {
			$wprp_ads_active_switch = get_post_meta( $post_ID, '_wprp_related_articles_active_switch', true );
			$wprp_widget_visibility	= get_post_meta( $post_ID, '_wprp_widget_visibility', true );
			$wprp_default_no_utm	= get_post_meta( $post_ID, '_wprp_default_no_utm', true );

	    @include  WPRP_PLUGIN_DIR . 'views/admin/template-custom-post-type-metabox-sidebar.php';
	  }

    if ( $column_name == 'shortcode' ) {
      echo '[wprp_related_posts id="' . $post_ID . '"]';
    }

    if ( $column_name == 'size' ) {
      echo get_post_meta( $post_ID, '_wprp_widget_size', true );
    }

		if ($column_name == 'displayon') {
	    echo get_post_meta($post_ID, "_wprp_device", true);
    }
	}

	private function _save_post_meta( $post, $wprp_meta ) {
		foreach ($wprp_meta as $key => $value) {
			if( $post->post_type == 'revision' ) {
				return;
			}

			if ( get_post_meta( $post->ID, $key, FALSE ) ) {
				update_post_meta( $post->ID, $key, $value );
			} else {
				add_post_meta( $post->ID, $key, $value );
			}

			if ( ! $value ) {
			 	delete_post_meta( $post->ID, $key );
			}
		}
	}
}
