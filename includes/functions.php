<?php
	function wraw_get_all_active_related_widgets()
	{
		$wraw_data = array(
			'post_type'		=> 'wraw_widgets',
			'relationship'	=> 'AND',
			'posts_per_page'		=> -1,
			'meta_query'	=> array(
				array(
					'key'		=> '_wraw_related_articles_active_switch',
					'value'		=> 1,
					'compare'	=> '='
				)
			)
		);

		$wraw_widgets = new WP_Query($wraw_data);

		return $wraw_widgets->posts;
	}