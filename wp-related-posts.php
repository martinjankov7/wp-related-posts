<?php
/**
 * Plugin Name: WP Related Articles Widgets
 * Description: Create widgets with realted articles.
 * Author:      Martin Jankov
 * Author URI:  https://martincv.com
 * Version:     0.0.1
 * Text Domain: wprp
 *
 * You should have received a copy of the GNU General Public License
 * along with WP Related Articles Widgets. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    WPRelatedPosts
 * @author     Martin Jankov
 * @since      1.0.0
 * @license    GPL-3.0+
 * @copyright  Copyright (c) 2018, Martin Jankov
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

final class WPRelatedPosts {

	private static $_instance;

	private $_version = '0.0.1';

	public $widget;

	public $related_articles_post_type;
	public $optimizer;
	public $rc_instream_ads;
	public $wp_widget;

	public static function instance() {

		if ( ! isset( self::$_instance ) && ! ( self::$_instance instanceof WPRelatedPosts ) ) {

			self::$_instance = new WPRelatedPosts;
			self::$_instance->constants();
			self::$_instance->includes();

			add_action( 'plugins_loaded', array( self::$_instance, 'objects' ), 10 );
			add_action( 'admin_enqueue_scripts', array( self::$_instance, 'load_global_admin_assets' ), 10 );
			add_action( 'wp_enqueue_scripts', array( self::$_instance, 'load_global_frontend_assets' ), 10 );
		}

		return self::$_instance;
	}

	private function includes() {

		require_once WPRP_PLUGIN_DIR . 'includes/functions.php';

		//Libraries
		if(!class_exists('Mobile_Detect') OR !function_exists('is_mobile'))
			require_once WPRP_PLUGIN_DIR . 'libraries/mobble/mobble.php';

		// Classes
		require_once WPRP_PLUGIN_DIR . 'classes/WPRP_External_Instream_Articles_Base.php';
		require_once WPRP_PLUGIN_DIR . 'classes/WPRP_RevContent_Instream.php';
		require_once WPRP_PLUGIN_DIR . 'classes/WPRP_Widget.php';
		require_once WPRP_PLUGIN_DIR . 'classes/widgets/WPRP_WP_Widget.php';

		// Admin/Dashboard only includes
		if ( is_admin() ) {
			require_once WPRP_PLUGIN_DIR . 'classes/admin/WPRP_Settings.php';
			require_once WPRP_PLUGIN_DIR . 'classes/admin/WPRP_Custom_Post_Type_Registration.php';
			require_once WPRP_PLUGIN_DIR . 'classes/admin/WPRP_Post_Metabox.php';
		}
	}

	private function constants() {

		// Plugin version
		if ( ! defined( 'WPRP_VERSION' ) ) {
			define( 'WPRP_VERSION', $this->_version );
		}

		// Plugin Folder Path
		if ( ! defined( 'WPRP_PLUGIN_DIR' ) ) {
			define( 'WPRP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL
		if ( ! defined( 'WPRP_PLUGIN_URL' ) ) {
			define( 'WPRP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File
		if ( ! defined( 'WPRP_PLUGIN_FILE' ) ) {
			define( 'WPRP_PLUGIN_FILE', __FILE__ );
		}
	}

	public function objects() {
		// Init classes if is Admin/Dashboard
		if ( is_admin() ) {
			new WPRP_Custom_Post_Type_Registration;
			new WPRP_Settings;
		}
	}

	public function load_global_admin_assets( $hook ){
		global $post;

		if ( ! isset( $post ) ) return;

		wp_register_style( 'wprp-admin-style', WPRP_PLUGIN_URL . 'assets/css/admin/style.css', array(), WPRP_VERSION );
		wp_register_script( 'wprp-admin-script', WPRP_PLUGIN_URL . 'assets/js/admin/script.js', array( 'jquery', 'wprp-selectize-script' ), WPRP_VERSION, true );
	}

	public function load_global_frontend_assets($hook){
		global $post;

		if ( ! isset( $post ) ) return;

		wp_register_style( 'wprp-style', WPRP_PLUGIN_URL . 'assets/css/style.css', array(), WPRP_VERSION);
		wp_register_script( 'wprp-script', WPRP_PLUGIN_URL . 'assets/js/script.js', array('jquery'), WPRP_VERSION, true );
	}
}

function wprp() {
	return WPRelatedPosts::instance();
}
wprp();
