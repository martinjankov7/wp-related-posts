<section id="wraw_wrapper">
	<div class="alert"></div>
	<section id="wraw_template">
		<label for="wraw_widget_revcontent_instream_count">RevContent instream ads: </label>
		<label for="wraw_widget_revcontent_instream_count">
			<input type="number" min="0" name="wraw_widget_revcontent_instream_count" placeholder="Instream ad count" value="<?php echo $wraw_widget_revcontent_instream_count; ?>">
		</label>
	</section>
	<section id="wraw_widget_language">
		<label for="wraw_widget_friendly_title_localization">Choose widget language: </label>
		<select id="wraw_widget_friendly_title_localization" name="wraw_widget_friendly_title_localization">
			<option value="en" <?php echo $wraw_widget_friendly_title_localization=='en' ? "selected" : ""; ?>>English</option>
			<option value="es" <?php echo $wraw_widget_friendly_title_localization=='es' ? "selected" : ""; ?>>Spanish</option>
			<option value="pt" <?php echo $wraw_widget_friendly_title_localization=='pt' ? "selected" : ""; ?>>Portugese</option>
		</select>
	</section>
	<section id="wraw_slides_number">
		<label for="wraw_widget_slides_number">Slides number to redirect to: </label>
		<label for="wraw_widget_slides_number">
			<input type="number" min="1" name="wraw_widget_slides_number" placeholder="Slides number" value="<?php echo $wraw_widget_slides_number; ?>">
		</label>
	</section>
	<section id="wraw_friendly_title">
		<label for="wraw_widget_friendly_title">Enter widget friendly title: </label>
		<input type="text" id="wraw_widget_friendly_title" name="wraw_widget_friendly_title" value="<?php echo $wraw_widget_friendly_title; ?>">
	</section>
	<section id="wraw_size">
		<label for="wraw_widget_size">Select Widget Size: </label>
		<select name="wraw_widget_size" id="wraw_widget_size">
			<?php
				 for($i = 1; $i <= 6; $i++) :
				 	for($j = 1; $j <= 6; $j++):
			?>
			<option value="<?php echo $i.'x'.$j; ?>" <?php echo $wraw_widget_size==$i.'x'.$j ? "selected" : ""; ?>><?php echo $i.'x'.$j; ?></option>
			<?php endfor; endfor; ?>
		</select>
	</section>
	<section id="wraw_template">
		<label for="wraw_widget_template">Select Template: </label>
		<select name="wraw_widget_template" id="wraw_widget_template">
			<option value="default" <?php echo $wraw_widget_template=='default' ? "selected" : ""; ?>>Default</option>
			<option value="wraw_rows" <?php echo $wraw_widget_template=='wraw_rows' ? "selected" : ""; ?>>Rows</option>
			<option value="slider_box" <?php echo $wraw_widget_template=='slider_box' ? "selected" : ""; ?>>Slider Box</option>
			<option value="1x1" <?php echo $wraw_widget_template=='1x1' ? "selected" : ""; ?>>1x1</option>
		</select>
	</section>
	<section id="wraw_action_button">
		<label for="wraw_action_button_text">Enter action button text: </label>
		<input type="text" id="wraw_action_button_text" name="wraw_action_button_text" value="<?php echo $wraw_action_button_text; ?>">
	</section>
	<section id="wraw_slider_box_position">
		<label>Select slider box position: </label>
		<label for="wraw_slider_box_left">
			<input type="radio" name="wraw_slider_box_position" id="wraw_slider_box_left" value="left" <?php echo $wraw_slider_box_position=="left" ? "checked" : ""; ?>> Bottom Left
		</label>
		<label for="wraw_slider_box_right">
			<input type="radio" name="wraw_slider_box_position" id="wraw_slider_box_right" value="right" <?php echo $wraw_slider_box_position=="right" ? "checked" : ""; ?>> Bottom Right
		</label>
	</section>
	<section id="wraw_device">
		<label>Select device: </label>
		<label for="wraw_device_desktop_tablet">
			<input type="radio" name="wraw_device" id="wraw_device_desktop_tablet" value="desktop_tablet" <?php echo $wraw_device=="desktop_tablet" ? "checked" : ""; ?>> Desktop
		</label>
		<label for="wraw_device_desktop_tablet">
			<input type="radio" name="wraw_device" id="wraw_device_tablet" value="tablet" <?php echo $wraw_device=="tablet" ? "checked" : ""; ?>> Tablet
		</label>
		<label for="wraw_device_mobile_tablet">
			<input type="radio" name="wraw_device" id="wraw_device_mobile" value="mobile" <?php echo $wraw_device=="mobile" ? "checked" : ""; ?>> Mobile
		</label>
		<label for="wraw_device_all">
			<input type="radio" name="wraw_device" id="wraw_device_all" value="all" <?php echo $wraw_device=="all" ? "checked" : ""; ?>> All
		</label>
	</section>
	<section id="wraw_widget_colors">
		<label for="wraw_widget_color">Widget's color: </label>
		<input type="color" id="wraw_widget_color" name="wraw_widget_color" value="<?php echo $wraw_widget_color ? $wraw_widget_color : ''; ?>">
	</section>
	<section id="wraw_articles_titles_length">
		<label for="wraw_widget_article_title_length">Enter article's title legth: </label>
		<input type="number" min="1" id="wraw_widget_article_title_length" name="wraw_widget_article_title_length" value="<?php echo $wraw_widget_article_title_length ? $wraw_widget_article_title_length : 65; ?>">
	</section>
	<section id="wraw_widgets_show_on_pages">
		<label for="wraw_widget_show_on_pages">
			Show After Page:
		</label>
		<label for="wraw_widget_show_on_pages_repeat">
			<input type="number" min="0" name="wraw_widget_show_on_pages" value="<?php echo $wraw_widget_show_on_pages ? $wraw_widget_show_on_pages: ''; ?>" placeholder="Show Widget After Page">
	    <input type="checkbox" name="wraw_widget_show_on_pages_repeat" class="wraw-widget-repeat" id="wraw_widget_show_on_pages_repeat" value="1" <?php checked($wraw_widget_show_on_pages_repeat,1); ?>> Repeat
		</label>
	</section>
	<section id="wraw_ads_show_utm_condtions">
		<label for="wraw_show_utm_condition">
			UTM Parameters:
		</label>
		<input type="text" name="wraw_utm_source" value="<?php echo $wraw_utm_source ? $wraw_utm_source: ''; ?>" placeholder="UTM Source">
		<input type="text" name="wraw_utm_medium" value="<?php echo $wraw_utm_medium ? $wraw_utm_medium: ''; ?>" placeholder="UTM Medium">
		<input type="text" name="wraw_utm_campaign" value="<?php echo $wraw_utm_campaign ? $wraw_utm_campaign: ''; ?>" placeholder="UTM Campaign">
	</section>
	<section id="wraw_widget_articles">
		<div class="wraw-action-buttons">
			<button type="button" id="wraw_add_titles" class="button button-primary button-medium"><span class="dashicons dashicons-thumbs-up"></span> Save Title & Posts</button>
			<div class="wraw-clear"></div>
		</div>
		<div class="wraw-in-widget-articles">
			<div class="wraw-label">Posts to show in widget</div>
			<button type="button" id="wraw_search_articles_btn" class="button button-primary button-medium"><span class="dashicons dashicons-search"></span> Search Articles</button>
			<button type="button" id="wraw_external_articles_btn" class="button button-primary button-medium"><span class="dashicons dashicons-pressthis"></span> Add External Article</button>
			<hr>
			<select name="wraw_widget_articles[]" class="selectize" id="wraw_articles" size="2" multiple>
				<?php foreach($wraw_widget_articles as $article_ID) :
						if(strpos($article_ID, ',')!==FALSE)
						{
							$article = explode(",",$article_ID);
							$title = $article[0];
							$value = $article_ID;
						}
						else
						{
							$title = get_the_title($article_ID);
							$value = $article_ID;
						}
					?>
					<option value="<?php echo $value; ?>" selected><?php echo $title; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="wraw-widget-on-articles">
			<div class="wraw-label">Posts to show the widget on</div>
			<button type="button" id="wraw_search_on_articles_btn" class="button button-primary button-medium"><span class="dashicons dashicons-search"></span> Search Articles</button>
			<hr>
			<select name="wraw_widget_on_articles[]" class="selectize" id="wraw_on_articles" size="2" multiple>
				<?php foreach($wraw_widget_on_articles as $article_ID) :
							$title = get_the_title($article_ID);
							$value = $article_ID;
					?>
					<option value="<?php echo $value; ?>" selected><?php echo $title; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</section>
</section>
<section class="wraw-external-modal">
	<?php @include WRAW_PLUGIN_DIR.'views/admin/template-external-article-modal.php'; ?>
</section>
