<div class="onoffswitch">
    <input type="checkbox" name="wraw_related_articles_active_switch" class="onoffswitch-checkbox widgets_status" data-widget-id="<?php echo $post_ID; ?>" id="myonoffswitch_<?php echo $post_ID; ?>" value="1" <?php checked($wraw_ads_active_switch,1); ?>>
    <label class="onoffswitch-label" for="myonoffswitch_<?php echo $post_ID; ?>">
        <span class="onoffswitch-inner"></span>
        <span class="onoffswitch-switch"></span>
    </label>
</div>
<label for="wraw_widget_visibility">
	<div class="onoffswitch">
	    <input type="checkbox" name="wraw_widget_visibility" class="onoffswitch-checkbox wraw_widget_visibility" data-widget-id="<?php echo $post_ID; ?>" id="myonoffswitch_wraw_wiget_visibility_<?php echo $post_ID; ?>" value="1" <?php checked($wraw_widget_visibility,1); ?>>
	    <label class="onoffswitch-label wraw_widget_visibility-label" for="myonoffswitch_wraw_wiget_visibility_<?php echo $post_ID; ?>">
	        <span class="onoffswitch-inner wraw_widget_visibility-inner"></span>
	        <span class="onoffswitch-switch wraw_widget_visibility-switch"></span>
	    </label>
	</div>
</label>
<label for="wraw_default_no_utm">
	<div class="onoffswitch">
	    <input type="checkbox" name="wraw_default_no_utm" class="onoffswitch-checkbox wraw_default_no_utm" data-widget-id="<?php echo $post_ID; ?>" id="myonoffswitch_wraw_default_no_utm_<?php echo $post_ID; ?>" value="1" <?php checked($wraw_default_no_utm,1); ?>>
	    <label class="onoffswitch-label wraw_default_no_utm-label" for="myonoffswitch_wraw_default_no_utm_<?php echo $post_ID; ?>">
	        <span class="onoffswitch-inner wraw_default_no_utm-inner"></span>
	        <span class="onoffswitch-switch wraw_default_no_utm-switch"></span>
	    </label>
	</div>
</label>