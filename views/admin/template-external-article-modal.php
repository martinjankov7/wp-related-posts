<div class="wraw-modal wraw-external-article">
	<h2>Add External Article</h2>
	<hr>
	<div class="wraw-main">
		<div class="wraw-input">
			<input type="text" placeholder="Enter article title" name="wraw_widget_external_article_title" value="<?php echo $wraw_widget_external_article_title; ?>">
		</div>
		<div class="wraw-input">
			<input type="text" placeholder="Enter article subtitle" name="wraw_widget_external_article_subtitle" value="<?php echo $wraw_widget_external_article_subtitle; ?>">
		</div>
		<div class="wraw-input">
			<input type="text" placeholder="Enter article link" name="wraw_widget_external_article_link" value="<?php echo $wraw_widget_external_article_link; ?>">
		</div>
		<div class="wraw-input">
			<input type="text" placeholder="Enter article source" name="wraw_widget_external_article_extra" value="<?php echo $wraw_widget_external_article_extra; ?>">
		</div>
		<div class="wraw-input">
			<button class="button button-primary wraw-select-image">Select Images</button>
			<div class="wraw-image-preview">
				<div class="wraw-row">
					<div class="wraw-external-article-image" style="background-image: url(<?php echo $image->url; ?>);">
						<span>X</span>
					</div>
					<input type="hidden" name="_wraw_external_article_image_src" value="<?php echo $image->url; ?>" >
	        		<input type="hidden" name="_wraw_external_article_image_id" value="<?php echo $image->id; ?>" >
				</div>
	        </div>
		</div>
		<div class="wraw-input">
			<button class="button button-primary wraw-external-article-add-button">Add Article</button>
			<button class="button button-default wraw-external-article-cancel-button">Cancel</button>
		</div>
	</div>
</div>