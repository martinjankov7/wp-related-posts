<?php 
	$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'revcontent';
?>
<div class="wrap">
	<div id="icon-tools" class="icon32"></div>
	<h2>Settings</h2>
	<hr/>
	<h2 class="nav-tab-wrapper">
	    <a href="?post_type=wraw_widgets&page=wraw-settings&tab=revcontent" class="nav-tab <?php echo $active_tab == 'revcontent' ? 'nav-tab-active' : ''; ?>">RevContent API</a>
	</h2>
	<form method="post" action="options.php"> 
		<?php settings_fields( 'wraw-settings-group' ); ?>
		<?php do_settings_sections( 'wraw-settings-group' ); ?>
		<?php settings_errors(); ?>
		<?php if($active_tab == 'revcontent')  @include (WRAW_PLUGIN_DIR . '/views/admin/partials/revcontent.php');  ?>
		<?php submit_button(); ?>
	</form>
</div>