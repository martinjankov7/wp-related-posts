<table class="form-table">
    <tr valign="top">
		<th scope="row">RevContent API Base Url</th>
        <td>
        	<input type="text" name="wraw_revcontent_api_base_url" size="50" value="<?php echo esc_attr( get_option('wraw_revcontent_api_base_url') ); ?>" required>
        </td>
    </tr>
    <tr>
        <th scope="row">RevContent API for Instream Ads</th>
        <td>
        	<input type="text" name="wraw_revcontent_api" size="50" value="<?php echo esc_attr( get_option('wraw_revcontent_api') ); ?>" required>
        </td>
    </tr>
    <tr>
        <th scope="row">RevContent Desktop WidgetID</th>
        <td>
        	<input type="text" name="wraw_revcontent_api_desktop_widget_id" size="50" value="<?php echo esc_attr( get_option('wraw_revcontent_api_desktop_widget_id') ); ?>" required>
        </td>
    </tr>
    <tr>
        <th scope="row">RevContent Mobile WidgetID</th>
        <td>
        	<input type="text" name="wraw_revcontent_api_mobile_widget_id" size="50" value="<?php echo esc_attr( get_option('wraw_revcontent_api_mobile_widget_id') ); ?>" required>
        </td>
    </tr>
    <tr>
        <th scope="row">RevContent Pub Id</th>
        <td>
        	<input type="text" name="wraw_revcontent_api_pub_id" size="50" value="<?php echo esc_attr( get_option('wraw_revcontent_api_pub_id') ); ?>" required>
        </td>
    </tr>
    <tr>
        <th scope="row">RevContent Domain</th>
        <td>
        	<input type="text" name="wraw_revcontent_api_domain" size="50" value="<?php echo esc_attr( get_option('wraw_revcontent_api_domain') ); ?>" required>
        </td>
    </tr>
</table>