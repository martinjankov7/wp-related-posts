<div class='wraw_localization'>
	<label for="wraw_widget_friendly_title">Select widget language: </label>
	<select id="wraw_widget_title_language" name="wraw_widget_title_language">
		<option value="en" <?php echo $wraw_widget_title_language=='en' ? "selected" : ""; ?>>English</option>
		<option value="es" <?php echo $wraw_widget_title_language=='es' ? "selected" : ""; ?>>Spanish</option>
		<option value="pt" <?php echo $wraw_widget_title_language=='pt' ? "selected" : ""; ?>>Portugese</option>
	</select>
</div>