<?php 
	if($this->_widget->slider_box_pos == "left")
	{
		$position = "left: -320px";
	}
	else
	{
		$position = "right: -320px";
	}
?>
<section class="wraw-widget-wrapper wraw-slider" id="<?php echo $this->_widget_id; ?>" style="<?php echo $position; ?>">
	<h1 class="wraw-title" align="center"><?php echo apply_filters('the_title',$this->_widget->title); ?></h1>
	<div class="wraw-row">
		<div class="wraw-slider-close">x</div>
		<?php foreach($articles as $article) : ?>
			<?php if($article->ID == get_the_ID()) continue; ?>
			<?php if($counter < $total_articles) : ?>
				<div class="wraw-slider-article-wrapper">
					<article id="<?php echo $article->ID; ?>">
						<a href="<?php echo $article->link; ?>" <?php if($article->ID == 0) echo "target='_blank'"; ?>>
							<div class="wraw-cols wraw-cols-2">
								<div class="wraw-image-responsive">
									<div class="wraw-image" style="background-image: url(<?php echo $article->image[0]; ?>);"></div>
								</div>
							</div>
							<div class="wraw-slider-article-title wraw-cols wraw-cols-2">
								<h1 class="wraw-h3"><?php echo (strlen($article->title) > $this->_widget->article_title_length) ? substr($article->title, 0, $this->_widget->article_title_length)."..." : $article->title; ?></h1>
							</div>
						</a>
						<div class="wraw-clearfix"></div>
						<span class="wraw-author">Worldlifestyle</span>
					</article>
				</div>
			<?php $counter++; endif; ?>
		<?php endforeach; ?>
	</div>
</section>