<section class="wraw-widget-wrapper" id="<?php echo $this->_widget_id; ?>">
	<h1 class="wraw-title" align="center"><?php echo apply_filters('the_title',$this->_widget->title); ?></h1>
	<div class="wraw-row">
		<?php foreach($articles as $article) : ?>
			<?php if($article->ID == get_the_ID()) continue; ?>
			<?php if($counter < $total_articles) : ?>
				<div class="wraw-cols wraw-cols-<?php echo $cols; ?>">
					<article id="<?php echo $article->ID; ?>">
						<a href="<?php echo $article->link; ?>" title="<?php echo $article->title; ?>" <?php if($article->ID == 0) echo "target='_blank'"; ?>>
							<div class="wraw-image-responsive">
								<?php echo $article->image_tag; ?>
							</div>
							<h1 class="wraw-h3"><?php echo (strlen($article->title) > $this->_widget->article_title_length) ? substr($article->title, 0, $this->_widget->article_title_length)."..." : $article->title; ?></h1>
						</a>
						<div class="wraw-clearfix"></div>
						<span class="wraw-author">Worldlifestyle</span>
					</article>
				</div>
			<?php $counter++; endif; ?>
		<?php endforeach; ?>
	</div>
</section>