<style>
	.wraw-rows-temp a:hover .wraw-temp1-content .wraw-heading{
	  color: <?php echo $this->_widget->color; ?>;
	}
	.wraw-rows-temp .wraw-row p {display:none;}
</style>
<section class="wraw-widget-wrapper wraw-rows-temp" id="<?php echo $this->_widget_id; ?>">
	<div class="wraw-title-wrapper" style="border-bottom: 2px solid <?php echo $this->_widget->color; ?>;">
		<h1 class="wraw-title" style="background:<?php echo $this->_widget->color; ?>;" align="center"><?php echo apply_filters('the_title',$this->_widget->title); ?></h1>
	</div>
		<?php foreach($articles as $article) : ?>
			<?php if($article->ID == get_the_ID()) continue; ?>
			<?php if($counter < $total_articles) : ?>
				<div class="wraw-row">
					<div class="wraw-cols wraw-cols-1">
						<article id="<?php echo $article->ID; ?>">
							<a href="<?php echo $article->link; ?>" title="<?php echo $article->title; ?>" <?php if($article->ID == 0) echo "target='_blank'"; ?>>
								<div class="wraw-cols wraw-cols-2 wraw-image-responsive wraw-rows">
									<div class="wraw-image" style="background-image: url(<?php echo $article->image[0]; ?>);"></div>
								</div>
								<div class="wraw-cols wraw-cols-2 wraw-temp1-content">
									<h1 class="wraw-heading <?php echo $this->_widget->font_100 ? 'wraw-h3' : 'wraw-h2' ?>">
										<?php echo (strlen($article->title) > $this->_widget->article_title_length) ? substr($article->title, 0, $this->_widget->article_title_length)."..." : $article->title; ?>
									</h1>
									<div class="wraw-clearfix"></div>
									<div class="wraw-author-date">
										<?php if(strlen($article->author)) : ?>
											<span class="wraw-author"><?php echo ucfirst($article->author); ?></span> <span class="wraw-date"> <?php echo $article->date; ?></span>
										<?php endif; ?>
										<?php if(isset($article->extra) && strlen($article->extra)) : ?>
											<span class="wraw-author"><?php echo $article->extra; ?></span>
											<span class="wraw-date wraw-extra-text"> <?php echo $article->extra; ?></span>
										<?php endif; ?>
									</div>
										<button class="wraw-read-more" style="background:<?php echo $this->_widget->color; ?>;">
											Read More <i class="fa fa-arrow-right"></i>
										</button>
								</div>
							</a><div class="wraw-clearfix"></div>							
						</article>
					</div>
				</div>
			<?php $counter++; endif; ?>
		<?php endforeach; ?>
	
</section>