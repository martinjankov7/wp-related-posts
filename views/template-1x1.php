<section class="wraw-widget-wrapper wraw-1x1" id="<?php echo $this->_widget_id; ?>">
	<?php if($this->_widget->title != '') : ?>
		<h1 class="wraw-title" align="center"><?php echo apply_filters('the_title',$this->_widget->title); ?></h1>
	<?php endif; ?>
	<div class="wraw-row">
		<?php foreach($articles as $article) : ?>
			<?php if($article->ID == get_the_ID()) continue; ?>
			<?php if($counter < $total_articles) : ?>
				<div class="wraw-1x1-article-wrapper">
					<article id="<?php echo $article->ID; ?>">
						<a href="<?php echo $article->link; ?>" <?php if($article->ID == 0) echo "target='_blank'"; ?>>
							<div class="wraw-cols wraw-cols-1">
								<div class="wraw-image-responsive">
									<div class="wraw-image" style="background-image: url(<?php echo $article->image[0]; ?>);"></div>
								</div>
							</div>
							<div class="wraw-meta">
								<div class="wraw-1x1-article-title wraw-cols wraw-cols-1">
									<h1 class="wraw-h3"><?php echo (strlen($article->title) > $this->_widget->article_title_length) ? substr($article->title, 0, $this->_widget->article_title_length)."..." : $article->title; ?></h1>
									<div class="wraw-clearfix"></div>
									<?php if(isset($article->subtitle) && strlen($article->subtitle)) : ?>
										<div class="wraw-date wraw-extra-text"> <?php echo esc_html($article->subtitle); ?></div>
									<?php endif; ?>
									<?php if(isset($article->extra) && strlen($article->extra)) : ?>
										<div class="wraw-date wraw-source-text"> <?php echo esc_html($article->extra); ?></div>
									<?php endif; ?>
								</div>
								<button class="wraw-action-button" style="background:<?php echo $this->_widget->color; ?>;"><?php echo $this->_widget->action_button_text; ?> <i class="fa fa-chevron-right round-arrow-right"></i></button>
								<div class="wraw-clearfix"></div>
							</div>
						</a>
					</article>
				</div>
			<?php $counter++; endif; ?>
		<?php endforeach; ?>
	</div>
</section>